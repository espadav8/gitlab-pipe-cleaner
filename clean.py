#!/usr/bin/env python3

import json
from urllib.request import urlopen, Request
from datetime import datetime, timedelta

# Needs API access
# https://gitlab.com/-/profile/personal_access_tokens?name=GitLab+Pipe+Cleaner&scopes=api
GITLAB_API_TOKEN=""
# Product ID to clean the pipelines from
GITLAB_PROJECT_ID=""
# Pipelines older than this will be deleted
DELETE_OLDER_THAN_DAYS=90
GITLAB_HOST="https://gitlab.com"
PIPELINE_LIST_URL=f"/api/v4/projects/{GITLAB_PROJECT_ID}/pipelines?order_by=id&sort=asc"
PIPELINE_DELETE_URL=f"/api/v4/projects/{GITLAB_PROJECT_ID}/pipelines/"
DELETE_BEFORE = (datetime.today() - timedelta(days=DELETE_OLDER_THAN_DAYS)).isoformat()

while True:
    httprequest = Request(f"{GITLAB_HOST}{PIPELINE_LIST_URL}", headers={"Accept": "application/json"})

    with urlopen(httprequest) as response:
        data = json.loads(response.read().decode())

        for pipeline in data:
            created_at = pipeline['created_at']
            pipeline_id = pipeline['id']

            if created_at < DELETE_BEFORE:
                deleterequest = Request(
                    f"{GITLAB_HOST}{PIPELINE_DELETE_URL}{pipeline_id}",
                    headers={
                        "Accept": "application/json",
                        "PRIVATE-TOKEN": GITLAB_API_TOKEN
                    },
                    method="DELETE"
                )
                print(f'delete this one: {pipeline_id}')
                urlopen(deleterequest)
            else:
                print(f"Don't delete this one: {pipeline_id}")
                print("Quitting")
                quit()
